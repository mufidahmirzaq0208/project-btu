import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FrontViewComponent } from './front-view/front-view.component';


const routes: Routes = 
[
  {path:'', component: FrontViewComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
